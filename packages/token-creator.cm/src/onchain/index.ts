import {
    contractRef,
    ContractId,
    getMethodArguments,
    toHex,
    getContractArgument,
    getContractId,
    continueTx,
    dataVerified,
    genericClaim,
    claimKey,
    contractIssuer,
    callLegacy,
    tokenizationBlockContractId,
    dataUnverified,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    executeHandler,
    TxContext,
    NewTx,
    HashId,
    getVerifiedClaimFrom,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

export const CREATE_METHOD_NAME = "CREATE";
export const CONGRATS_METHOD_NAME = "CONGRATS";

// TODO: add ContractCall into contract-kit
export function callMeBack(self: HashId, tokenName: string): any {
    return {
        contract_input: {
            authenticated: false,
            cost: "0x000000000000000000000000000000000000000000000000000000000007a120",
            data: [
                CONGRATS_METHOD_NAME,
                {
                    name: tokenName,
                },
            ],
        },
        contract_ref: contractRef(self),
    };
}

function createToken(contextTx: TxContext): NewTx[] {
    const tokenName = getMethodArguments(contextTx)[1];
    const tokenContent = getMethodArguments(contextTx)[2];
    const initialTokenSupply = toHex(getMethodArguments(contextTx)[3]);
    for (let i = 1; i < contextTx.ops.length; i++) {
        console.log(JSON.stringify(getContractArgument(contextTx, i)));
    }
    let self = getContractId(contextTx);
    const ecdsaIssuer = { FromSmartContract: "EcdsaContract" };
    const ecdsaAuth = getVerifiedClaimFrom(contextTx, ecdsaIssuer);
    if (!ecdsaAuth) {
        throw "Ecdsa contract verified claim should be provided";
    }
    // Our account which we use for signing
    const account = ecdsaAuth.body[1];
    const data = {
        msg: {
            cmd: {
                CreateTokenUi: {
                    extra_fields: [
                        {
                            content: tokenContent,
                            name: "content",
                        },
                        {
                            content: tokenName,
                            name: "name",
                        },
                    ],
                    memo: "00000000-0000-0000-0000-000000000000",
                    protocol_fields: {
                        initial_token_supply: initialTokenSupply,
                        issuance_type: {
                            Mintable: {
                                minters: [
                                    {
                                        auth: "EcdsaContract",
                                        payload: account,
                                    },
                                ],
                            },
                        },
                        token_admin: {
                            auth: "EcdsaContract",
                            payload: account,
                        },
                    },
                },
            },
            on_failure: null,
            on_success: callMeBack(self, tokenName),
        },
        to_broadcaster: null,
    };

    return [
        continueTx([
            dataVerified(
                genericClaim(claimKey(null, null), null, toHex(1000000)),
                contractIssuer(self),
            ),
            callLegacy(2, tokenizationBlockContractId()),
            dataUnverified(data),
            // Forward ECDSA auth claim
            dataVerified(ecdsaAuth, ecdsaIssuer),
        ]),
    ];
}

function congratsOnCreateToken(contextTx: TxContext): NewTx[] {
    const tokenName = getMethodArguments(contextTx)[1].name;
    console.log("Congrats! You have created", tokenName);
    return [];
}

export function cwebMain() {
    addMethodHandler(CREATE_METHOD_NAME, createToken);
    addMethodHandler(CONGRATS_METHOD_NAME, congratsOnCreateToken);
    addMethodHandler(SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(contextTx);
}
