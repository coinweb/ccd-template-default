import {
    TxContext,
    NewTx,
    getContractId,
    continueTx,
    passCwebFrom,
    contractIssuer,
    store,
    genericClaim,
    claimKey,
    toHex,
    addDefaultMethodHandler,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    executeHandler,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

function logic(contextTx: TxContext): NewTx[] {
    const issuer = getContractId(contextTx);
    console.log("Congrats with new hello world!");
    return [
        continueTx([
            passCwebFrom(contractIssuer(issuer), 200),
            store(
                genericClaim(
                    // Key
                    claimKey(1, 4),
                    // Value
                    "hello world",
                    // Fees stored in this claim
                    toHex(0),
                ),
            ),
        ]),
    ];
}

export function cwebMain() {
    addDefaultMethodHandler(logic);
    addMethodHandler(SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(contextTx);
}
